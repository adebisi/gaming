# pokemon
Strategies

### Capture a Azelf

I was playing on Omega Ruby and went to a mystery cavern. I tried to capture it at first but, it wiped out my party and lost 7k. I used lapras(lv 44), Krookodile(lv 40), Blaziken(lv 63), Bellossom(lv 24) and raichu( lv 52). I bought a bunch of hyper potions and revives just in case. I also bought 10 dusk and 10 timer pokeballs. My plan was to use Krookodile mud attack to lower Azelf's accuracy but, that attack does not work on it and it killed my pokemon right away. I then send lapras since it was strong against its attack in my previous encounter. I used water pulse 3 times and lower its HP to almost dead. I switched out lapras since it was almost dead and took out raichu. I used  nuzzle for a small damage and paralyzed it. In which it worked! raichu was strong against it attacks so I started trowing dusk pokeball and it finally worked after the third try!

### Capture a Bagon Location: Meteor Falls B1F 2R

You can find Bagon where the dragon claw is located.
There is a issue with small space where Bagon spawns. Use the shark pokemon with surf and use a mega repel. The trick is to get on the water with surf
and go from the top to bottom and back and forth. This forces for the wild pokemon to spawn on the land. You will need to be careful when you get on land or
you might scare him away. Get on land, sneak and catch! sometimes you go so fast from the water to the land that you auto battle bagon.

